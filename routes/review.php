<?php

use App\Http\Controllers\ReviewController;
use Illuminate\Support\Facades\Route;


Route::post('/brewery/{brewery_id}/beer/{beer_id}/review', [ReviewController::class, 'create'])
    ->middleware(['auth'])
    ->name('review.create');

Route::get('/brewery/{brewery_id}/beer/{beer_id}/review', [ReviewController::class, 'form'])
    ->middleware(['auth'])
    ->name('review.form');

<?php

use App\Http\Controllers\BeerController;
use Illuminate\Support\Facades\Route;

Route::get('/beer', [BeerController::class, 'index'])
    ->middleware(['auth'])
    ->name('beer.index');

Route::get('/brewery/{brewery_id}/beer/{beer_id}', [BeerController::class, 'show'])
    ->middleware(['auth'])
    ->name('beer.show');

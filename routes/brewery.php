<?php

use App\Http\Controllers\BreweryController;
use Illuminate\Support\Facades\Route;

Route::get('/brewery', [BreweryController::class, 'index'])
    ->middleware(['auth'])
    ->name('brewery.index');

Route::get('/brewery/{id}', [BreweryController::class, 'show'])
    ->middleware(['auth'])
    ->name('brewery.show');

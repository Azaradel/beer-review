<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Beer;
use App\Models\Brewery;
use App\Models\User;
use App\Models\Review;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::factory()
            ->count(10)
            ->create();

        Brewery::factory()
            ->count(rand(4, 10))
            ->create();

        Beer::factory()
            ->count(rand(10, 30))
            ->create();

        Review::factory()
            ->count(rand(30, 50))
            ->create();
    }
}

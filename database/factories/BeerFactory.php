<?php

namespace Database\Factories;

use App\Models\Beer;
use App\Models\Brewery;
use Illuminate\Database\Eloquent\Factories\Factory;

class BeerFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Beer::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $filepath = public_path('images');
        $breweries = Brewery::pluck('id')->toArray();

        return [
            'name' => $this->faker->sentence(rand(1, 3)),
            'type' => $this->faker->sentence(rand(1, 3)),
            'description' => $this->faker->sentence(20, 30),
            'image' => $this->faker->image($filepath, 95, 285, 'animals', false),
            'brewery_id' => $this->faker->randomElement($breweries),
            'created_at' => now(),
            'updated_at' => now(),
        ];
    }
}

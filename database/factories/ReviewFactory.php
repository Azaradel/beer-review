<?php

namespace Database\Factories;

use App\Models\Beer;
use App\Models\User;
use App\Models\Review;
use Illuminate\Database\Eloquent\Factories\Factory;

class ReviewFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Review::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $beers = Beer::pluck('id')->toArray();
        $users = User::pluck('id')->toArray();

        return [
            'text' => $this->faker->sentence(20, 50),
            'rating' => $this->faker->numberBetween(1, 5),
            'beer_id' => $this->faker->randomElement($beers),
            'user_id' => $this->faker->randomElement($users),
            'created_at' => now(),
            'updated_at' => now(),
        ];
    }
}

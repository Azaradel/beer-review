<?php

use App\Models\Beer;
use App\Models\Brewery;
use App\Models\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBeersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('breweries', function (Blueprint $table) {
            $table->id();
            $table->string('name', 64);
            $table->text('description')->nullable();
            $table->timestamps();
        });

        Schema::create('beers', function (Blueprint $table) {
            $table->id();
            $table->string('name', 64);
            $table->text('description')->nullable();
            $table->string('type', 100)->nullable();
            $table->string('image');
            $table->timestamps();
            $table->foreignIdFor(Brewery::class);
        });

        Schema::create('reviews', function (Blueprint $table) {
            $table->id();
            $table->text('text')->nullable();
            $table->integer('rating', unsigned: true);
            $table->timestamps();
            $table->foreignIdFor(User::class);
            $table->foreignIdFor(Beer::class);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reviews');
        Schema::dropIfExists('beers');
        Schema::dropIfExists('breweries');
    }
}

<x-app-layout>

    <div class="max-w-7xl mx-auto sm:px-6 lg:px-8 border-t border-gray-200">
        <div class="bg-gray-50 shadow overflow-hidden sm:rounded-lg">
            <div class="px-4 py-5 sm:px-6">
                <h3 class="text-lg leading-6 font-medium text-gray-900">
                    Reviews
                </h3>
                <p class="mt-1 max-w-2xl text-sm text-gray-500">
                    User opinions about specific beer
                </p>
            </div>
            @foreach($beer->reviews()->get() as $review)
            <dl>
                <div class="bg-white px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6 items-center">
                    <blockquote class="col">
                        <p class="font-semibold text-center">
                            "{{ $review->text }}"
                        </p>
                    </blockquote>
                    <figcaption>
                        <div class="text-gray-500">
                            {{ $review->user->name }}
                        </div>
                    </figcaption>
                    <div class="font-semibold">
                        {{ $review->rating }}/5
                    </div>
                </div>
            </dl>

            @endforeach
        </div>
</x-app-layout>
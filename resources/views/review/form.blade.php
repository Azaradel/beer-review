<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Review') }}
        </h2>
    </x-slot>

    <div class="py-12 -my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
        <div class=" mt-5 md:mt-0 md:col-span-2 max-w-7xl mx-auto sm:px-6 lg:px-8">
            <form method="POST" action="{{ route('review.create', ['brewery_id' => $brewery_id, 'beer_id' => $beer_id]) }}">
                @csrf
                <div class="shadow sm:rounded-md sm:overflow-hidden">
                    <div class="px-4 py-5 bg-white space-y-6 sm:p-6">
                        <div>
                            <label for="review_text" class="block text-sm font-medium text-gray-700">
                                Review
                            </label>
                            <div class="mt-1">
                                <textarea id="review_text" name="review_text" rows="3" class="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 mt-1 block w-full sm:text-sm border-gray-300 rounded-md" placeholder="The composition of tastes..."></textarea>
                            </div>
                            <p class="mt-2 text-sm text-gray-500">
                                You summary of reviewed beer.
                            </p>
                        </div>

                        <div class="col-span-6 sm:col-span-3">
                            <label for="review_rating" class="block text-sm font-medium text-gray-700">Rating</label>
                            <select id="review_rating" name="review_rating" class="mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
                                <option>5</option>
                                <option>4</option>
                                <option>3</option>
                                <option>2</option>
                                <option>1</option>
                            </select>
                        </div>
                    </div>
                    <div class="px-4 py-3 bg-gray-50 text-right sm:px-6">
                        <button type="submit" class="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                            Save
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</x-app-layout>
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Beer extends Model
{
    use HasFactory;

    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'type',
        'description'
    ];

    public function averageRating()
    {
        $reviews = $this->reviews()->get();
        $count = count($reviews);

        if ($count === 0) {
            return 0;
        }

        $avg = 0;
        foreach ($reviews as $review) {
            $avg = $review->rating + $avg;
        }

        return $avg / $count;
    }

    public function brewery()
    {
        return $this->belongsTo(Brewery::class);
    }

    public function reviews()
    {
        return $this->hasMany(Review::class);
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\Beer;
use App\Models\Review;
use Illuminate\Http\Request;

class ReviewController extends Controller
{
    public function form($brewery_id, $beer_id)
    {
        return view('review.form', [
            'beer_id' => $beer_id,
            'brewery_id' => $brewery_id
        ]);
    }

    public function create(Request $request, $brewery_id, $beer_id)
    {
        $beer = Beer::findOrFail($beer_id);

        $review = new Review();

        $review->text = $request->review_text;
        $review->rating = $request->review_rating;
        $review->user_id = $request->user()->id;
        $review->beer_id = $beer_id;

        $review->save();

        return redirect(route('beer.show', [
            'brewery_id' => $beer->brewery_id, 'beer_id' => $beer->id
        ]));
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\Beer;
use Illuminate\Http\Request;

class BeerController extends Controller
{
    public function index()
    {
        $beers = Beer::orderBy('id', 'desc')->paginate(10);
        return view('beer.index', [
            'beers' => $beers,
        ]);
    }

    public function show($brewery_id, $beer_id)
    {
        $beer = Beer::findOrFail($beer_id);

        return view(
            'beer.show',
            [
                'beer' => $beer
            ]
        );
    }
}

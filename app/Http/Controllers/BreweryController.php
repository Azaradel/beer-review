<?php

namespace App\Http\Controllers;

use App\Models\Brewery;
use Illuminate\Http\Request;

class BreweryController extends Controller
{
    public function index()
    {
        $breweries = Brewery::orderBy('id', 'desc')->paginate(10);
        return view('brewery.index', [
            'breweries' => $breweries,
        ]);
    }

    public function show($id)
    {
        $brewery = Brewery::findOrFail($id);
        $beers = [];

        if ($brewery) {
            $beers = $brewery->beers;
        }

        return view(
            'brewery.show',
            [
                'brewery' => $brewery,
                'beers' => $beers
            ]
        );
    }
}
